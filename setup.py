from setuptools import setup

setup(
    name='proyecto-devops',
    version='1',
    packages=['pyttsx3',
              'requests',
              'wikipedia',
              'pywhatkit'],
    license=' GNU AFFERO GENERAL PUBLIC LICENSE V3',
    author='Erik Alcantara',
    author_email='erik.alcova@gmail.com',
    description='Sourcecode for Cassius'
)
