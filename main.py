"""Import all the necesary libraries"""
import sys
import os
from datetime import datetime
from random import choice
from pprint import pprint
import speech_recognition as sr
import pyttsx3
from utils import opening_text
from online_ops import get_random_advice, get_random_joke, play_on_youtube, search_on_google, search_on_wikipedia, send_whatsapp_message

USERNAME = "valhir"
BOTNAME = "Cassius"


engine = pyttsx3.init()

# Set Rate
engine.setProperty('rate', 150)

# Set Volume
engine.setProperty('volume', 1.0)

# Set Voice
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[24].id)

paths = {
    'discord': "/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=discord com.discordapp.Discord > /dev/null 2>&1 disown %1"
}
def speak(text):
    """Used to speak whatever text is passed to it"""
    engine.say(text)
    engine.runAndWait()
def greet_user():
    """Greets the user according to the time"""
    hour = datetime.now().hour
    if 6 >= hour < 12:
        speak(f"Good Morning {USERNAME}")
    elif 12 >= hour < 16:
        speak(f"Good afternoon {USERNAME}")
    elif 16 >= hour < 19:
        speak(f"Good Evening {USERNAME}")
    speak(f"I am {BOTNAME}. How may I assist you?")
def take_user_input():
    """Takes user input, recognizes it using Speech Recognition module and converts it into text"""
    user = sr.Recognizer()
    with sr.Microphone() as source:
        print('Listening....')
        user.pause_threshold = 1
        audio = user.listen(source)
    try:
        print('Recognizing...')
        query = user.recognize_google(audio, language='en-us')
        if 'exit' not in query or 'stop' in query:
            speak(choice(opening_text))
        else:
            hour = datetime.now().hour
            if 21 >= hour < 6:
                speak("Good night sir, take care!")
            else:
                speak('Have a good day sir!')
            sys.exit()
    except UnboundLocalError:
        speak('Sorry, I could not understand. Could you please say that again?')
        query = 'None'
    return query
def open_discord():
    """Opens discord on linux systems using flatpak"""
    os.system(paths['discord'])
if __name__ == '__main__':
    greet_user()
    while True:
        user_voice = take_user_input().lower()
        if 'wikipedia' in user_voice:
            speak('What do you want to search on Wikipedia, sir?')
            search_query = take_user_input().lower()
            results = search_on_wikipedia(search_query)
            speak(f"According to Wikipedia, {results}")
            speak("For your convenience, I am printing it on the screen sir.")
            print(results)
        elif 'youtube' in user_voice:
            speak('What do you want to play on Youtube, sir?')
            video = take_user_input().lower()
            play_on_youtube(video)
        elif 'search on google' in user_voice:
            speak('What do you want to search on Google, sir?')
            google = take_user_input().lower()
            search_on_google(google)
        elif "send whatsapp message" in user_voice:
            speak('On what number should I send the message sir? Please enter in the console: ')
            number = input("Enter the number: ")
            speak("What is the message sir?")
            message = take_user_input().lower()
            send_whatsapp_message(number, message)
            speak("I've sent the message sir.")
        elif 'joke' in user_voice:
            speak("Hope you like this one sir")
            joke = get_random_joke()
            speak(joke)
            speak("For your convenience, I am printing it on the screen sir.")
            pprint(joke)
        elif "advice" in user_voice:
            speak("Here's an advice for you, sir")
            advice = get_random_advice()
            speak(advice)
            speak("For your convenience, I am printing it on the screen sir.")
            pprint(advice)
        else:
            speak("Anything else I could do?")
